var element = document.getElementById("text");
console.log(element);
console.log(element.innerText);
console.log(element.innerHTML);
console.log(element.getAttribute('id'));

element.innerText = "Hello!";

function openAlert(name) {
	alert("Hi " + name + "!");
}

function sayHi() {
	var inputElement = document.getElementById("name-input");
	var pElement = document.getElementById("text");
	pElement.innerText = "Hello, " + inputElement.value;
}