$(document).ready(function() {
	var currentIndex = 0;
	
	var images = [
		"https://i.ytimg.com/vi/kYnx7nPw-xo/maxresdefault.jpg",
		"https://www.petakids.com/wp-content/uploads/2015/11/Cute-Red-Bunny.jpg",
		"https://i.ytimg.com/vi/Lv4SQy_9VLI/maxresdefault.jpg",
		"https://cdn.shopify.com/s/files/1/1377/8215/products/bunny-rabbit-zoo-snood-dog-snood-costume-20788108993_1024x1024.jpg?v=1508895561",
		"http://cdn.kickvick.com/wp-content/uploads/2015/09/cutest-bunny-rabbits-23.jpg"
	];

	$("#btn-next").click(next);

	$("#btn-prev").click(function() {
		if (currentIndex === 0) {
			currentIndex = images.length - 1;
		} else {
			currentIndex--;
		}

		$("#slider-image").fadeOut(function() {
			$("#slider-image").attr("src", images[currentIndex]);
			$("#slider-image").fadeIn();	
		});
	})

	$("#slider-image").click(next);

	function next() {
		if (currentIndex === images.length - 1) {
			currentIndex = 0;
		} else {
			++currentIndex;
		}

		$("#slider-image").fadeOut(function() {
			$("#slider-image").attr("src", images[currentIndex]);
			$("#slider-image").fadeIn();
		});
	}

	interval = setInterval(function() {
		next();
	}, 2000);

	$("#btn-hold").click(function() {
		clearInterval(interval);
	})

	$("#btn-start-again").click(function() {
		interval = setInterval(function() {
			next();
		}, 2000);
	})

});