$(document).ready(function() {
	$('#home-link').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#main-slider").offset().top
		}, 800);

		$('.nav-item').removeClass('active');
		$('#home-link').parent().addClass('active');
	});


	$('#services-link').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#services").offset().top
		}, 800);

		$('.nav-item').removeClass('active');
		$('#services-link').parent().addClass('active');
	});


	$('#about-link').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#about").offset().top
		}, 800);

		$('.nav-item').removeClass('active');
		$('#about-link').parent().addClass('active');
	});


	$('#contacts-link').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#contact-us").offset().top
		}, 800);

		$('.nav-item').removeClass('active');
		$('#contacts-link').parent().addClass('active');
	});
});